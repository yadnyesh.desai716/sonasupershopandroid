package com.example.sonasupershop.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sonasupershop.R;

public class CustomerItemHolder extends RecyclerView.ViewHolder {

    TextView custName_tv, custAddress_tv, custMobile_tv;

    public CustomerItemHolder(@NonNull View itemView) {
        super(itemView);

        custName_tv = itemView.findViewById(R.id.custName_tv);
        custAddress_tv = itemView.findViewById(R.id.custAddress_tv);
        custMobile_tv = itemView.findViewById(R.id.custMobile_tv);
    }
}
