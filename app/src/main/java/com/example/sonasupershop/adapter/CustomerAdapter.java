package com.example.sonasupershop.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sonasupershop.R;
import com.example.sonasupershop.model.Customer;

import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerItemHolder> {

    private final List<Customer> customerList;

    public CustomerAdapter(List<Customer> customerList) {
        this.customerList = customerList;
    }

    @NonNull
    @Override
    public CustomerItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_customer_item, parent, false);

        return new CustomerItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerItemHolder holder, int position) {
        Customer customer = customerList.get(position);
        holder.custName_tv.setText(customer.getName());
        holder.custAddress_tv.setText(customer.getAddress());
        holder.custMobile_tv.setText(String.valueOf(customer.getMobile()));
    }

    @Override
    public int getItemCount() {
        return customerList.size();
    }
}
