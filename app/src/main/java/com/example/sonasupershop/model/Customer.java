package com.example.sonasupershop.model;

import java.util.List;
import java.util.Objects;


public class Customer {

    private Integer id;

    private String name;

    private Long mobile;

    private String address;

    private List<Order> orders;

    public Customer() {
    }

    public Customer(Integer id, String name, Long mobile, String address, List<Order> orders) {
        this.id = id;
        this.name = name;
        this.mobile = mobile;
        this.address = address;
        this.orders = orders;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMobile() {
        return mobile;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "cust_id=" + id +
                ", nameEt='" + name + '\'' +
                ", mobileEt=" + mobile +
                ", addressEt='" + address + '\'' +
                ", orders=" + orders +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (!(obj instanceof Customer customer))
            return false;

        return Objects.equals(id, customer.id) &&
                Objects.equals(name, customer.name) &&
                Objects.equals(mobile, customer.mobile) &&
                Objects.equals(address, customer.address) &&
                Objects.equals(orders, customer.orders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, mobile, address, orders);
    }
}
