package com.example.sonasupershop.model;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class Order {

   private Integer id;

    private Customer customer;

    private List<Product> products;

    private LocalDate order_date;

    private boolean delivered;

    public Order() {
    }

    public Order(Integer id, Customer customer, List<Product> products, LocalDate order_date, boolean delivered) {
        this.id = id;
        this.customer = customer;
        this.products = products;
        this.order_date = order_date;
        this.delivered = delivered;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public LocalDate getOrder_date() {
        return order_date;
    }

    public void setOrder_date(LocalDate order_date) {
        this.order_date = order_date;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", customer=" + customer +
                ", products=" + products +
                ", date=" + order_date +
                ", delivered=" + delivered +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Order order))
            return false;

        return delivered == order.delivered &&
                Objects.equals(id, order.id) &&
                Objects.equals(customer, order.customer) &&
                Objects.equals(products, order.products) &&
                Objects.equals(order_date, order.order_date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customer, products, order_date, delivered);
    }
}
