package com.example.sonasupershop.retrofit;

import com.example.sonasupershop.model.Customer;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface CustomerApi {

    @GET("/list-all-customers")
    Call<List<Customer>> getAllCustomers();

    @POST("/add-customer")
    Call<Customer> addCustomer(@Body Customer customer);
}
