package com.example.sonasupershop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sonasupershop.activity.ListCustomersActivity;
import com.example.sonasupershop.activity.StartActivity;
import com.example.sonasupershop.model.Customer;
import com.example.sonasupershop.retrofit.CustomerApi;
import com.example.sonasupershop.retrofit.RetrofitService;

import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeComponents();
    }

    private void initializeComponents() {
        EditText nameEt = findViewById(R.id.nameEt);
        EditText mobileEt = findViewById(R.id.mobileEt);
        EditText addressEt = findViewById(R.id.addressEt);
        Button signUpBtn = findViewById(R.id.signUpBtn);

        //  Send the Customer object through the network
        RetrofitService retrofitService = new RetrofitService();
        CustomerApi customerApi = retrofitService.getRetrofit().create(CustomerApi.class);

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEt.getText().toString().trim();
                String mobile = mobileEt.getText().toString().trim();
                String address = addressEt.getText().toString().trim();

                Customer customer = new Customer();
                customer.setName(name);
                customer.setMobile(Long.valueOf(mobile));
                customer.setAddress(address);

                customerApi.addCustomer(customer)
                        .enqueue(new Callback<Customer>() {
                            @Override
                            public void onResponse(Call<Customer> call, Response<Customer> response) {

                                if (!response.isSuccessful()) {
                                    if (response.code() >= 400 && response.code() < 500)
                                        Toast.makeText(MainActivity.this, "Not allowed", Toast.LENGTH_SHORT).show();
                                    if (response.code() >= 500 && response.code() < 600)
                                        Toast.makeText(MainActivity.this, "Internal Server Error", Toast.LENGTH_SHORT).show();

                                    return;
                                }

                                Toast.makeText(MainActivity.this, "User signed up successfully", Toast.LENGTH_SHORT).show();
                                Intent intentMain = new Intent(MainActivity.this, ListCustomersActivity.class);
                                MainActivity.this.startActivity(intentMain);
//                                MainActivity.this.finish();
                            }

                            @Override
                            public void onFailure(Call<Customer> call, Throwable t) {
                                Toast.makeText(MainActivity.this, "Error occured, please try again...", Toast.LENGTH_SHORT).show();
                                Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, "Error occured", t);
                            }
                        });
            }
        });
    }
}