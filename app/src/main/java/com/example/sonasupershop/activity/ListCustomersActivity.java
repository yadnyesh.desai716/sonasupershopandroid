package com.example.sonasupershop.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.example.sonasupershop.R;
import com.example.sonasupershop.adapter.CustomerAdapter;
import com.example.sonasupershop.model.Customer;
import com.example.sonasupershop.retrofit.CustomerApi;
import com.example.sonasupershop.retrofit.RetrofitService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListCustomersActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_customers);

        recyclerView = findViewById(R.id.custList_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        loadCustomers();
    }

    private void loadCustomers() {

        RetrofitService retrofitService = new RetrofitService();
        CustomerApi customerApi = retrofitService.getRetrofit().create(CustomerApi.class);
        customerApi.getAllCustomers()
                .enqueue(new Callback<List<Customer>>() {
                    @Override
                    public void onResponse(Call<List<Customer>> call, Response<List<Customer>> response) {
                        populateView(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Customer>> call, Throwable t) {
                        Toast.makeText(ListCustomersActivity.this, "Failed to load customers, please try again...", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void populateView(List<Customer> customerList) {

        CustomerAdapter customerAdapter = new CustomerAdapter(customerList);
        recyclerView.setAdapter(customerAdapter);
    }
}