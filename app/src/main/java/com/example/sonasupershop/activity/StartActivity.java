package com.example.sonasupershop.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.sonasupershop.MainActivity;
import com.example.sonasupershop.R;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(() -> {
            Intent intentMain = new Intent(StartActivity.this, MainActivity.class);
            StartActivity.this.startActivity(intentMain);
            StartActivity.this.finish();
        }, 2000);

    }
}